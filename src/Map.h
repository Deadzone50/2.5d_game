#pragma once
#include "Vector/Vector.h"
#include <vector>

struct Wall
{
	uint32_t Vert1, Vert2;
	uint32_t Neighbour = 0;
};

struct Sector
{
	std::vector<Wall> Walls;
	float CeilH, FloorH;
};

void InitMap(std::vector<Vec2f> &Points, std::vector<Sector> &Map);
