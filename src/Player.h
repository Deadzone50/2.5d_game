#pragma once
#include "Vector/Vector.h"

class Player
{
public:
	Vec3f GetPos();
	float GetAngle();
	uint32_t GetSector();
	float GetEyeHeight();

	void Move(float F, float R, float U);
	void Turn(float Angle);

	void SetPos(float x, float y);
	void SetSector(uint32_t SectorIndex);

private:
	Vec3f Pos_ = {0,0,0};
	float Angle_ = 0;
	uint32_t Sector_ = 1;
	float EyeHeight_ = 1.8f;
};
