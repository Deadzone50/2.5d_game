#include "Vector.h"

#include <cmath>
#include <string>
#include <sstream>

bool
operator==(const Vec3i &V1, const Vec3i &V2)
{
	if(V1.x == V2.x)
		if(V1.y == V2.y)
			if(V1.z == V2.z)
				return true;
	return false;
}
Vec3i operator*(const Vec3i &V1, const Vec3i &V2)
{
	Vec3i V;
	V.x = V1.x*V2.x;
	V.y = V1.y*V2.y;
	V.z = V1.z*V2.z;
	return V;
}
Vec3i operator+(const Vec3i &V1, const Vec3i &V2)
{
	Vec3i V;
	V.x = V1.x+V2.x;
	V.y = V1.y+V2.y;
	V.z = V1.z+V2.z;
	return V;
}
Vec3i operator-(const Vec3i &V1, const Vec3i &V2)
{
	Vec3i V;
	V.x = V1.x-V2.x;
	V.y = V1.y-V2.y;
	V.z = V1.z-V2.z;
	return V;
}
void operator*=(Vec3i &V1, const Vec3i &V2)
{
	V1.x *= V2.x;
	V1.y *= V2.y;
	V1.z *= V2.z;
}
void operator+=(Vec3i &V1, const Vec3i &V2)
{
	V1.x += V2.x;
	V1.y += V2.y;
	V1.z += V2.z;
}
void operator-=(Vec3i &V1, const Vec3i &V2)
{
	V1.x -= V2.x;
	V1.y -= V2.y;
	V1.z -= V2.z;
}

Vec3f operator*(float f, const Vec3i &V1)
{
	Vec3f V;
	V.x=f*V1.x;
	V.y=f*V1.y;
	V.z=f*V1.z;
	return V;
}
Vec3f operator*(const Vec3i & V, float f)	{return f*V;}
Vec3i operator*(int i, const Vec3i &V1)
{
	Vec3i V;
	V.x=i*V1.x;
	V.y=i*V1.y;
	V.z=i*V1.z;
	return V;
}
Vec3i operator*(const Vec3i & V, int i)		{return i*V;}
Vec3f operator/(const Vec3i &V1, float f)
{
	Vec3f V;
	V.x=V1.x/f;
	V.y=V1.y/f;
	V.z=V1.z/f;
	return V;
}
Vec3i operator-(const Vec3i &V1)
{
	Vec3i V;
	V.x = -V1.x;
	V.y = -V1.y;
	V.z = -V1.z;
	return V;
}

std::ostream& operator<<(std::ostream &os, const Vec3i &V)
{
	os << V.x << " , " << V.y << " , " << V.z;
	return os;
}
std::istream& operator>>(std::istream &is, Vec3i &V)
{
	is >> V.x >> V.y >> V.z;
	return is;
}