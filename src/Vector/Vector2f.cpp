#include "Vector.h"

#include <cmath>
#include <string>
#include <sstream>

bool
operator==(const Vec2f &V1, const Vec2f &V2)
{
	if(V1.x == V2.x)
		if(V1.y == V2.y)
			return true;
	return false;
}
Vec2f operator*(const Vec2f &V1, const Vec2f &V2)
{
	Vec2f V;
	V.x = V1.x*V2.x;
	V.y = V1.y*V2.y;
	return V;
}
Vec2f operator+(const Vec2f &V1, const Vec2f &V2)
{
	Vec2f V;
	V.x = V1.x+V2.x;
	V.y = V1.y+V2.y;
	return V;
}
Vec2f operator-(const Vec2f &V1, const Vec2f &V2)
{
	Vec2f V;
	V.x = V1.x-V2.x;
	V.y = V1.y-V2.y;
	return V;
}
void operator*=(Vec2f &V1, const Vec2f &V2)
{
	V1.x *= V2.x;
	V1.y *= V2.y;
}
void operator+=(Vec2f &V1, const Vec2f &V2)
{
	V1.x += V2.x;
	V1.y += V2.y;
}
void operator-=(Vec2f &V1, const Vec2f &V2)
{
	V1.x -= V2.x;
	V1.y -= V2.y;
}

Vec2f operator*(const Vec2f &V1, const Vec2i &V2)
{
	Vec2f V;
	V.x = V1.x*V2.x;
	V.y = V1.y*V2.y;
	return V;
}
Vec2f operator*(const Vec2i &V1, const Vec2f &V2)	{return V2*V1;}

Vec2f operator*(float f, const Vec2f &V1)
{
	Vec2f V;
	V.x=f*V1.x;
	V.y=f*V1.y;
	return V;
}
Vec2f operator*(const Vec2f & V, float f)	{return f*V;}
Vec2f operator*(int i, const Vec2f &V1)
{
	Vec2f V;
	V.x=i*V1.x;
	V.y=i*V1.y;
	return V;
}
Vec2f operator*(const Vec2f & V, int i)		{return i*V;}
Vec2f operator/(const Vec2f &V1, float f)
{
	Vec2f V;
	V.x=V1.x/f;
	V.y=V1.y/f;
	return V;
}
Vec2f operator-(const Vec2f &V1)
{
	Vec2f V;
	V.x = -V1.x;
	V.y = -V1.y;
	return V;
}

std::ostream& operator<<(std::ostream &os, const Vec2f &V)
{
	os << V.x << " , " << V.y;
	return os;
}
std::istream& operator>>(std::istream &is, Vec2f &V)
{
	is >> V.x >> V.y;
	return is;
}