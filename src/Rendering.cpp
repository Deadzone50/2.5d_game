#include <utility>
#include "Rendering.h"

void DrawPoint(uint32_t x, uint32_t y, Vec3f Color, std::vector<Vec3f> &Screen)
{
	Screen[y*SCREENW + x] = Color;
}

void DrawVerticalLine(uint32_t x, uint32_t y1, uint32_t y2, Vec3f Color, std::vector<Vec3f> &Screen)
{
	if(y2 < 0 || y1 >= SCREENH || x < 0 || x >= SCREENW)
		return;
	if(y1 < 0)
		y1 = 0;
	if(y2 >= SCREENH)
		y2 = SCREENH-1;

	for(int y = y1; y < y2; ++y)
	{
		Screen[y*SCREENW + x] = Color;
	}
}

void DrawLine(Vec2f Start, Vec2f End, Vec3f Color, std::vector<Vec3f> &Screen)
{
	Vec2f Dir = End-Start;
	if(fabsf(Dir.x) > fabsf(Dir.y))
	{
		if(Start.x > End.x)
			{
				std::swap(Start, End);
				Dir = End-Start;
			}
		float StepY = (float)Dir.y/Dir.x;
		for(uint32_t x = 0; (x < Dir.x) && (x+Start.x < SCREENW); ++x)
		{
			int32_t X = Start.x+x;
			int32_t Y = Start.y + StepY*x;
			if((0 <= X && X < SCREENW) && (0 <= Y && Y < SCREENH))
				Screen[Y*SCREENW + X] = Color;
		}
	}
	else
	{
		if(Start.y > End.y)
		{
			std::swap(Start, End);
			Dir = End-Start;
		}
		float StepX = (float)Dir.x/Dir.y;
		for(uint32_t y = 0; (y < Dir.y) && (y+Start.y < SCREENH); ++y)
		{
			int32_t X = Start.x+StepX*y;
			int32_t Y = Start.y+y;
			if((0 <= X && X < SCREENW) && (0 <= Y && Y < SCREENH))
				Screen[Y*SCREENW + X] = Color;
		}
	}
}

void DrawWall(Wall &W, std::vector<Vec2f> &Points, std::vector<Vec3f> &Screen)
{

}

void DrawSector(Sector &S, std::vector<Vec2f> &Points, std::vector<Vec3f> &Screen)
{

}

Vec2f Intersect(Vec2f P1, Vec2f P2, Vec2f P3, Vec2f P4)
{ 
	return Vec2f{CROSS(Vec2f{CROSS(P1, P2), P1.x-P2.x}, Vec2f{CROSS(P3, P4), (P3.x)-(P4.x)}) / 
	CROSS(Vec2f{P1.x-P2.x, P1.y-P2.y}, Vec2f{P3.x-P4.x, P3.y-P4.y}),
	
	CROSS(Vec2f{CROSS(P1, P2), P1.y-P2.y}, Vec2f{CROSS(Vec2f{P3.x,P3.y}, Vec2f{P4.x,P4.y}), P3.y-P4.y}) /
	CROSS(Vec2f{P1.x-P2.x, P1.y-P2.y}, Vec2f{P3.x-P4.x, P3.y-P4.y})};
}

void DrawSectorsToScreen(std::vector<Sector> &Map, std::vector<Vec2f> &Points, std::vector<Vec3f> &Screen, Player &Player)
{
	Sector CurrentSector = Map[Player.GetSector()];
	float Angle = Player.GetAngle();

	Vec2f Center = {SCREENW/2, SCREENH/2};
	Vec2f MapCenter = {SCREENW/2, 50};
	float MapScale = 1.f;
	Vec2f PlayerPos = {Player.GetPos().x, Player.GetPos().y};
	float FovH = 90.0f/180.0f*3.14f;	//rad

	float NearPlaneY = 0.01f;
	float NearPlaneX = NearPlaneY*tanf(FovH/2);
	float FarPlaneY = 50.0f;
	Vec2f NearLeft = {-NearPlaneX, NearPlaneY};
	Vec2f NearRight = {NearPlaneX, NearPlaneY};
	Vec2f FarLeft = {-FarPlaneY*tanf(FovH/2), FarPlaneY};
	Vec2f FarRight = {FarPlaneY*tanf(FovH/2), FarPlaneY};

	float FovV = FovH*SCREENW/SCREENH;
	float NearPlaneH = NearPlaneY*tanf(FovV/2);
	float WallHeight = 10;

	DrawLine(MapCenter, MapCenter+Vec2f{0,20}, {1, 0, 0}, Screen);	//map player
	DrawLine(MapCenter, MapCenter+Vec2f{0,2}, {1, 1, 1}, Screen);

	DrawLine(MapCenter+NearLeft*MapScale, MapCenter+FarLeft*MapScale, {1, 1, 1}, Screen);	//map frustum
	DrawLine(MapCenter+NearRight*MapScale, MapCenter+FarRight*MapScale, {1, 1, 1}, Screen);

	for(auto Wall : CurrentSector.Walls)
	{
		uint64_t x = (Wall.Vert1*7+Wall.Vert2*71);	//xorshift
		x ^= x >> 12;
		x ^= x << 25;
		x ^= x >> 27;
		x *= (uint64_t)1181783497276652981;
		float r = (((uint8_t)(x>>0))%101)/100.0f;
		float g = (((uint8_t)(x>>8))%101)/100.0f;
		float b = (((uint8_t)(x>>16))%101)/100.0f;

		Vec2f WallS = Points[Wall.Vert1];
		Vec2f WallE = Points[Wall.Vert2];
		//translate to player relative
		Vec2f TranslatedS = WallS - PlayerPos;
		Vec2f TranslatedE = WallE - PlayerPos;
		//rotate around player
		Vec2f RotatedS = {TranslatedS.x*cosf(Angle) - TranslatedS.y*sinf(Angle), TranslatedS.y*cosf(Angle) + TranslatedS.x*sinf(Angle)};
		Vec2f RotatedE = {TranslatedE.x*cosf(Angle) - TranslatedE.y*sinf(Angle), TranslatedE.y*cosf(Angle) + TranslatedE.x*sinf(Angle)};

		DrawLine(MapCenter+RotatedS*MapScale, MapCenter+RotatedE*MapScale, 0.3f*Vec3f{r, g, b}, Screen);	//map

		if(RotatedS.y < NearPlaneY && RotatedE.y < NearPlaneY)	//completly behind player
			continue;

		float CosFrustrum = DOT({0,1}, NORMALIZE(FarRight-NearRight));
		bool OutsideS = (DOT({0,1}, NORMALIZE(RotatedS)) < CosFrustrum );
		bool OutsideE = (DOT({0,1}, NORMALIZE(RotatedE)) < CosFrustrum );
		if(OutsideS || OutsideE)		//one or both ends outside frustrum
		{
			//Right frustrum
			Vec2f A = NearRight;
			Vec2f V = (FarRight-NearRight);
			Vec2f C = RotatedS;
			Vec2f W = (RotatedE-RotatedS);

			Vec2f PL, PR;
			float t1R = (A-C).x*V.y - (A-C).y*V.x;
			float div1R = W.x*V.y - V.x*W.y;
			t1R/=div1R;
			if((RotatedS + t1R*W).y < NearPlaneY)	//throw away if behind player
				t1R = -1;

			//Left frustrum
			A = NearLeft;
			V = (FarLeft-NearLeft);
			float t1L = (A-C).x*V.y - (A-C).y*V.x;
			float div1L = W.x*V.y - V.x*W.y;
			t1L/=div1L;
			if((RotatedS + t1L*W).y < NearPlaneY)	//throw away if behind player
				t1L = -1;

			if(OutsideE && OutsideS &&
				(0 <= t1R && t1R <= 1) &&
				(0 <= t1L && t1L <= 1))		//both outside, but infront of player
			{
				RotatedE = RotatedS + t1R*W;
				RotatedS = RotatedS + t1L*W;
			}
			else if(0 <= t1R && t1R <= 1)
			{
				if(OutsideE)
				{
					RotatedE = RotatedS + t1R*W;
				}
				else
				{
					RotatedS = RotatedS + t1R*W;
				}
			}
			else if(0 <= t1L && t1L <= 1)
			{
				if(OutsideE)
				{
					RotatedE = RotatedS + t1L*W;
				}
				else
				{
					RotatedS = RotatedS + t1L*W;
				}
			}
			else
				continue;	//both end on same side
		}

		DrawLine(MapCenter+RotatedS*MapScale, MapCenter+RotatedE*MapScale, {r, g, b}, Screen);	//visible map


		if(RotatedS.x > RotatedE.x)
			std::swap(RotatedS, RotatedE);

		//project to nearplane
		float ProjectedSx = RotatedS.x*(NearPlaneY/RotatedS.y);
		float ProjectedEx = RotatedE.x*(NearPlaneY/RotatedE.y);
		//precent of nearplane width
		float SxPercentOfNear = ProjectedSx/NearPlaneX;
		float ExPercentOfNear = ProjectedEx/NearPlaneX;

		//Absolute x position on screen
		float SxScreen = (SCREENW/2)*SxPercentOfNear + SCREENW/2;
		float ExScreen = (SCREENW/2)*ExPercentOfNear + SCREENW/2;

		float DistS = RotatedS.y;	//seem better with this than actual distance
		float DistE = RotatedE.y;

		float HeightS = NearPlaneY/DistS * WallHeight/NearPlaneH;	//percent of screen height
		float HeightE = NearPlaneY/DistE * WallHeight/NearPlaneH;

		if(SxScreen > ExScreen)
		{
			std::swap(SxScreen, ExScreen);
			std::swap(HeightS, HeightE);
		}

		int32_t StartX = SxScreen;
		int32_t EndX = ExScreen;

		if(StartX < 0)	//clip outside of screen
		{
			StartX = 0;
		}
		if (EndX >= SCREENW)
		{
			EndX = SCREENW-1;
		}

		for(int32_t x = StartX; x < EndX; ++x)	//need to be int if endx is negative
		{
			float x_percent = (x-SxScreen)/(ExScreen-SxScreen);
			float Height = (HeightE-HeightS)*x_percent + HeightS;
			int32_t y1 = Center.y - Height*SCREENH/2;
			int32_t y2 = Center.y + Height*SCREENH/2;
			if(y1 < 0) y1 = 0;
			if(y2 >= SCREENH) y2 = SCREENH-1;
			DrawVerticalLine(x, y1, y2, {r,g,b}, Screen);
		}
	}
	//Draw roof
	//Draw floor
}
