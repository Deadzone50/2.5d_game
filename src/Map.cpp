#include "Map.h"

void InitMap(std::vector<Vec2f> &Points, std::vector<Sector> &Map)
{
	Points = 
	{
		{10,10}, {60,10},
		{60,110}, {160,110},
		{160,160}, {10,160}
	};
	Map =
	{
		{
			{{0,1},{1,2},{2,5,2},{5,0}},
			10, 0
		},
		{
			{{2,3},{3,4},{4,5},{5,2,1}},
			12, 2
		}
	};
}