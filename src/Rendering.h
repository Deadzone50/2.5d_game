#pragma once
#include <stdint.h>
#include <vector>
#include "Vector/Vector.h"
#include "Map.h"
#include "Player.h"

#define SCREENW 800
#define SCREENH 600

void DrawPoint(uint32_t x, uint32_t y, Vec3f Color, std::vector<Vec3f> &Screen);
void DrawVerticalLine(uint32_t x, uint32_t y1, uint32_t y2, Vec3f Color, std::vector<Vec3f> &Screen);

void DrawSectorsToScreen(std::vector<Sector> &Map, std::vector<Vec2f> &Points, std::vector<Vec3f> &Screen, Player &Player);
