//#include <GL/glew.h>
#include <GL/glut.h>
#define _USE_MATH_DEFINES
#include <math.h>
//#include <ctime>
#include <chrono>
#include <random>
#include <iostream>

#include "Vector/Vector.h"
#include "Player.h"
#include "Map.h"
#include "Rendering.h"


Player PlayerObj;
bool Keys[256] = {false};
bool KeysSpecial[256] = {false};
std::chrono::high_resolution_clock::time_point tOld = std::chrono::high_resolution_clock::now();
std::mt19937 RandomGen(0);  // mt19937 is a standard mersenne_twister_engine
std::vector<Vec2f> Points;
std::vector<Sector> Map;
std::vector<Vec3f> Screen = std::vector<Vec3f>(SCREENH*SCREENW);

Vec2f ScaleVector(Vec2f p)
{
	Vec2f V;
	V.x = 2*p.x/SCREENW-1;
	V.y = 2*p.y/SCREENH-1;
	return V;
}

void RenderScreen(std::vector<Vec3f> &Screen)
{
	glDrawPixels(SCREENW, SCREENH, GL_RGB, GL_FLOAT, &Screen[0]);
}

void KeyBoardDown(unsigned char key, int x, int y)
{
	Keys[key] = true;
}
void KeyBoardUp(unsigned char key, int x, int y)
{
	Keys[key] = false;
}

void KeyBoardSpecialDown(int key, int x, int y)
{
	KeysSpecial[key] = true;
}
void KeyBoardSpecialUp(int key, int x, int y)
{
	KeysSpecial[key] = false;
}

void HandleKeys(float dt)
{
	if(Keys['q'])
	{
		exit(1);
	}
	float Forward = 0;
	float Right = 0;
	float Speed = 50;
	float TurnSpeed = 1.0f;
	//if(Keys['w'])
	//{
	//	Forward = Speed*dt;
	//}
	//else if(Keys['s'])
	//{
	//	Forward = -Speed*dt;
	//}
	//if(Keys['a'])
	//{
	//	Right = -Speed*dt;
	//}
	//else if(Keys['d'])
	//{
	//	Right = Speed*dt;
	//}
	float Angle = 0;
	if(KeysSpecial[GLUT_KEY_UP])
	{
		Forward = Speed*dt;
	}
	else if(KeysSpecial[GLUT_KEY_DOWN])
	{
		Forward = -Speed*dt;
	}
	if(KeysSpecial[GLUT_KEY_LEFT])
	{
		Angle = -TurnSpeed*dt;
	}
	else if(KeysSpecial[GLUT_KEY_RIGHT])
	{
		Angle = TurnSpeed*dt;
	}
	if(Angle)
		PlayerObj.Turn(Angle);
	if(Forward || Right)
		PlayerObj.Move(Forward, Right, 0);
}

void DrawMap()
{
	glBegin(GL_LINES);
	for(auto S : Map)
	for(auto W : S.Walls)
	{
		uint64_t x = (W.Vert1*7+W.Vert2*71);	//xorshift
		x ^= x >> 12;
		x ^= x << 25;
		x ^= x >> 27;
		x *= (uint64_t)1181783497276652981;
		float r = (((uint8_t)(x>>0))%101)/100.0f;
		float g = (((uint8_t)(x>>8))%101)/100.0f;
		float b = (((uint8_t)(x>>16))%101)/100.0f;
		glColor3f(r,g,b);
		Vec2f S = ScaleVector(Points[W.Vert1]);
		Vec2f E = ScaleVector(Points[W.Vert2]);
		glVertex2f(S.x, S.y);
		glVertex2f(E.x, E.y);
	}
	glEnd();
}
uint8_t Counter = 0;
void MainLoop(void)
{
	std::chrono::high_resolution_clock::time_point tNow = std::chrono::high_resolution_clock::now();
	float dt = std::chrono::duration_cast<std::chrono::duration<float>>(tNow-tOld).count();
	if(Counter++ == 20)
	{
		std::cout << dt << " ms, FPS: " << 1.0f/dt << "\n";
		Counter = 0;
	}
	HandleKeys(dt);
	tOld = tNow;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	DrawMap();
	DrawSectorsToScreen(Map, Points, Screen, PlayerObj);
	RenderScreen(Screen);

	//glColor3f(1,1,0);
	//glBegin(GL_LINES);
	//Vec2f Pos = ScaleVector({PlayerObj.GetPos().x, PlayerObj.GetPos().y});
	//float Angle = PlayerObj.GetAngle();
	//glVertex2f(Pos.x, Pos.y);
	//glVertex2f((Pos.x+50.0f/SCREENW*sinf(Angle)), (Pos.y+50.0f/SCREENH*cosf(Angle)));
	//glEnd();
	//glColor3f(1,0,0);
	//glBegin(GL_POINTS);
	//glVertex2f(Pos.x, Pos.y);
	//glEnd();

	glutSwapBuffers();
	memset(&Screen[0], 0, SCREENH*SCREENW*sizeof(Vec3f));
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(SCREENW, SCREENH);
	glutCreateWindow("2.5D");

	glutDisplayFunc(MainLoop);
	glutIdleFunc(MainLoop);

	glutKeyboardFunc(KeyBoardDown);
	glutKeyboardUpFunc(KeyBoardUp);

	glutSpecialFunc(KeyBoardSpecialDown);
	glutSpecialUpFunc(KeyBoardSpecialUp);

	PlayerObj.SetPos(30, 30);
	InitMap(Points, Map);

	glutMainLoop();
	return 1;
}
