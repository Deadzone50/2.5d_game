#define _USE_MATH_DEFINES
#include <math.h>

#include "Player.h"

Vec3f Player::GetPos()
{
	return Pos_;
}
float Player::GetAngle()
{
	return Angle_;
}
uint32_t Player::GetSector()
{
	return Sector_;
}
float Player::GetEyeHeight()
{
	return EyeHeight_;
}
void Player::Move(float F, float R, float U)
{
	Pos_.x += sinf(Angle_)*F + cosf(Angle_)*R;
	Pos_.y += cosf(Angle_)*F - sinf(Angle_)*R;
	Pos_.z += U;
}

void Player::Turn(float Angle)
{
	Angle_ += Angle;
	if(Angle_ > (2*M_PI))
	{
		Angle_ -= 2*M_PI;
	}
	if(Angle_ < 0)
	{
		Angle_ += 2*M_PI;
	}
}

void Player::SetPos(float x, float y)
{
	Pos_.x = x;
	Pos_.y = y;
}
void Player::SetSector(uint32_t Sector)
{
	Sector_ = Sector;
}
